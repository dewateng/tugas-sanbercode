1. 	create database myshop

2.	create table users(
    -> 	id int(8) auto_increment,
    -> 	name varchar(255),
    -> 	email varchar(255),
    -> 	password varchar(255),
    -> 	primary key(id)
    -> 	);

	create table categories(
    -> 	id int(8) auto_increment,
    -> 	name varchar(255),
    -> 	primary key(id)
    -> 	);

	create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(20),
    -> category_id int(8),
    -> primary key(id),
    -> foreign key(category_id) refrences categories(id)
    -> );

3. 	insert into users(name,email,password) values ('John Doe','john@doe.com','john123');
   	insert into users(name,email,password) values ('Jane Doe','jane@doe.com','jenita123');

	insert into categories(name) values ('gadget'),('cloth'),('men'),('women'),('branded');

	insert into items(name, description, price, stock, category_id) 
	values ('Sumsang b50','hape keren dari merek sumsang','4000000','100','1'),
	('Uniklooh','baju keren dari merek ternama','500000','50','2'),
	('IMHO Watch','jam tangan anak yang jujur banget','2000000','10','1');

4(a).	select id, name, email from users;

4(b). 	select * from items where price > 1000000;
	select * from items where name like '%sang%';

4(c).	select items.name, 
	items.description,
	items.price, 
	items.stock, 
	items.category_id, 
	categories.name as kategori 
	from items 
	left join categories 
	on items.category_id = categories.id;

5	update items
	set price = 2500000
	where name like '%sumsang%';